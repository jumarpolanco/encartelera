angular.module('site', ['ui.router', 'cartelera', 'cine'])
        .config(['$stateProvider', function ($stateProvider) {
                //states for this module
                $stateProvider
                        .state('site', {
                            url: '/',
                            templateUrl: 'modules/site/index.html',
                            controller: function () {
                            }
                        });
            }])
        .run(function ($rootScope) {
            $rootScope.$on('$viewContentLoaded', function () {
                $(document).foundation();
            });
        });
;

