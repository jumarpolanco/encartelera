angular.module('cine', [])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
                //states for this module
                $stateProvider
                        .state('site.cines', {
                            url: 'cines',
                            templateUrl: 'modules/site/cine/list.html',
                            controller: 'CinesController'
                        })
                        .state('site.cine', {
                            url: 'cines/:idcine',
                            templateUrl: 'modules/site/cine/details.html',
                            controller: 'CineController'
                        });
            }])
        .controller('CinesController', ['$scope', '$cine', function ($scope, $cine) {
                $scope.cines = $cine.list;
            }])
        .controller('CineController', ['$scope', '$cine', '$stateParams', function ($scope, $cine, $stateParams) {
                $scope.cine = $cine.get($stateParams.idcine);
            }])
;