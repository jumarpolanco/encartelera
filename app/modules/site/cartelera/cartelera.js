angular.module('cartelera', [])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
                //this is the homepage module
                $urlRouterProvider.otherwise("/cartelera");

                //states for this module
                $stateProvider
                        .state('site.cartelera', {
                            url: 'cartelera',
                            templateUrl: 'modules/site/cartelera/list.html',
                            controller: 'CarteleraListController'
                        })
                        .state('site.pelicula', {
                            url: 'cartelera/:idpelicula',
                            templateUrl: 'modules/site/cartelera/details.html',
                            controller: 'CarteleraDetailController'
                        });
            }])
        .controller('CarteleraListController', ['$scope', '$pelicula', function ($scope, $pelicula) {
                $scope.peliculas = $pelicula.list;
            }])
        .controller('CarteleraDetailController', function ($scope, $stateParams, $pelicula) {
            $scope.pelicula = $pelicula.get($stateParams.idpelicula);
        });

