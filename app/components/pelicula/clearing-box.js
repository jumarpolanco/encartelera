(function () {
    'use strict';

    angular.module('mm.foundation.clearingbox', [])
            .directive('clearingList', function () {
                return{
                    restrict: 'EA',
                    transclude: 'true',
                    replace: 'false',
                    template: '<ul class="clearing-thumbs" data-clearing ng-transclude></ul>'                    
                }
            })
            .directive('clearingImg', function () {
                return{
                    require: ['clearingList', 'src'],
                    restrict:'AE',
                    replace:'true',
                    template:'<li><a href="{{src}}"><img src="{{src}}"/></a></li>',
                    scope: {
                        src:'='
                    }
                }
            });
})();

