(function () {
    'use strict';

    angular.module('enCine', [
        'site'
                , 'mm.foundation'
                , 'firebase'
                , 'angular.filter'
                , 'mm.foundation.clearingbox'
    ])
            .constant('FIREBASE_URL', 'http://encineapp.firebaseio.com');
})();